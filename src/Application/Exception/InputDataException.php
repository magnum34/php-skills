<?php



namespace Application\Exception;



class InputDataException extends \DomainException {
    
    public function ErrorMessage($request,$message){
        $request->getSession()->getFlashBag()->add('error',$message);
    }
}
