<?php

namespace Application\ExchangeRate\Provider\Settings;

class Language {
    const PL = 'PL';
    const EN = 'EN';
}
