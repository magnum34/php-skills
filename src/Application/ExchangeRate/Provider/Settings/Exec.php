<?php

namespace Application\ExchangeRate\Provider\Settings;
use Application\ExchangeRate\Provider\Settings\Language;

class Exec {
    
    const METHOD = array(Language::PL => "przelicz",  Language::EN => "convert");

    const BETWEEN = array(Language::PL => "na",  Language::EN => "to");
}
