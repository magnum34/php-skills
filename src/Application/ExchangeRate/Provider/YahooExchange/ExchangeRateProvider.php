<?php

namespace Application\ExchangeRate\Provider\YahooExchange;


/**
 *Mainly class provider exchange currency yahoo
 */
abstract class ExchangeRateProvider {
        
    protected $currencyBase;
    /**
     *Exchange Rate
     * @var float
     */
    protected $rate;
    /**
     *Base currency code
     * @var string
     */
    protected $currencyIn;
    /**
     *Quoted currency code
     * @var string 
     */
    protected $currencyOut;
    /**
    *@param string $currencyIn
    * @param string $currencyOut 
    */
    abstract public function fetch($currencyIn, $currencyOut);
    /**
     * Exchange Rate
     * @return float
     */
    abstract public function getRateValue();
    abstract public function setBaseCurrency($currencyValue);
    abstract public function getBaseCurrency();
    /**
     * Get exchange rate with Yahoo from file xml 
     * @param string $currencyIn
     * @param string $currencyOut
     * @return float
     */
    public function Load($currencyIn,$currencyOut){
        $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in(%22" . strtolower($currencyIn) . strtolower($currencyOut) . "%22)&env=store://datatables.org/alltableswithkeys";
        $xml = simplexml_load_file($url);
        return floatval($xml->results->rate[0]->Rate);
    }
}
