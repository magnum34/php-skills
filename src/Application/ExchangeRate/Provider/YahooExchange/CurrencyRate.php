<?php



namespace Application\ExchangeRate\Provider\YahooExchange;
use Application\ExchangeRate\Provider\YahooExchange\ExchangeRateProvider;
/**
 * Currency calculator
 */
class CurrencyRate  extends ExchangeRateProvider{
    /**
     * Get exchange rate with Yahoo from file xml 
     * @param string $currencyIn
     * @param string $currencyOut
     * @return \Application\ExchangeRate\Provider\YahooExchange\CurrencyRate
     */
    public function fetch($currencyIn, $currencyOut) {
       $this->rate = parent::Load($currencyIn, $currencyOut);
       return $this;
    }
    /**
     * get base currency
     * @return type
     */
    public function getBaseCurrency() {
        return $this->currencyBase;
    }
    /**
     * get rate currency
     * @return float
     */
    public function getRateValue() {
        return $this->rate;
    }
    /**
     * Currency calculator
     * @param float $currencyValue
     * @return float
     */
    public function setBaseCurrency($currencyValue) {
        $this->currencyBase = $currencyValue;
        return $this->rate*$currencyValue;
    }

}
