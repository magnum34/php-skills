<?php



namespace Application\ExchangeRate\Provider\RegularMatchExchange;
use Application\Exception\InputDataException;
/**
 *Number argument.
 *Check that value is correct.
 *Share on integer and decimal.
 *Required format USA e.g 12.00 No(12,00) -> Decimal mark
 * https://en.wikipedia.org/wiki/Decimal_mark
 */
class ArgumentAmount {
   
    private $integer = 0;
    
    private $all = 0;
    
    private $decimal = 0;
    
    private $amount = '';
    
    public function __construct($amount = null) {
       if(strlen($amount) != 0 && $amount != null){
           $this->init($amount);
       }     
    }
    
    public function init($amount){
       $this->amount = $amount;
       $this->Share($this->amount);
    }
    /**
     * Share on integer and decimal.
     * @param string $amount
     * @throws InputDataException
     */
    public function Share($amount){
        try{
            $this->isCorrectAmount($amount);
            $amount = number_format($amount, 2, '.', '');
            $share = explode(".", $amount);
            if(count($share) == 2){
                $this->integer = (int)$share[0];
                $this->decimal = (int)$share[1];
                $this->all = $amount;
            }elseif(count($share) == 1){
                $this->integer = (int)$share[0];
                $this->decimal = 0;
                $this->all = (float)$amount;
                
            }else{
                throw new InputDataException("The problem of converting strings to integer or float");
            }
                 
        } catch (Exception $ex) {
            throw new InputDataException("The problem of converting strings to integer or float");
        }
    }
    /**
     * Check that value is correct.
     * @param string $amount
     * @throws InputDataException
     */
    private function isCorrectAmount($amount){
        $share = explode(",", $amount); 
        if(floatval($amount) == 0){
            throw new InputDataException("Empty or Null amount ");
        }
        if(count($share)> 1){
            throw new InputDataException("Amount in wrong format only 'USA' ");
        }
        
    }
    public function getInteger(){
        return $this->integer;
    }
    public function  getDecimal(){
        return $this->decimal;
    }
    /**
     * connection integer with decimal
     * @return float
     */
    public function  getAll(){
        return $this->all;
    }
}
