<?php

namespace Application\ExchangeRate\Provider\RegularMatchExchange;
use Application\Exception\InputDataException;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentConvert;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentCurrency;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentAmount;
use Application\ExchangeRate\Provider\RegularMatchExchange\MultipleCurrency;

use Application\ExchangeRate\Provider\YahooExchange\CurrencyRate;
/**
 * Simple currency conversion string
 * Create Argument Definition
 * An argument, also called a command line argument, is a file name or other data that 
 * is provided to a command in order for the command to use it as an input.
 * A command is an instruction telling a computer to do something, such as execute (i.e., run) a program.
 * 
 * Unique argument or command: convert, przelicz, to, na 
 */
class RegularMatchExchange {
    
    /**
     * Collection arguments in bracket
     * @var array()
     */
    private $array = array();
    /**
     *All arguments program
     * @var array()
     */
    private $args = array();
    
    
    public function __construct() {
        
    }
    /**
     * Conversion data nad match string
     * Single Currency EN: convert GBP 10050 to EUR
     * Single Currency PL: przelicz GBP 10050 na EUR
     * Multiple Currency EN: convert ("GBP 4000", "PLN 350") to ("EUR","GBP")
     * Multiple Currency PL: przelicz ("GBP 4000", "PLN 350") na ("EUR","GBP")
     * 
     * @param string $input
     * @return array(array(ArgumentCurrency),array(ArgumentAmount),multiple)
     * @throws InputDataException
     */
    public function getDataArray($input){
        $input = strtoupper($input);
        preg_match_all('/\(([^\)]*)\)/', $input, $this->array);
        $this->array = $this->array[1];

        if(count($this->array) > 0 && count($this->array) <= 2){
            $from = $this->array[0];
            $array_converter = $this->array[1];

            $this->clearMultipleArgs($input);
            if(count($this->args) == 2){
                $method = new ArgumentConvert($this->args[0],$this->args[1]);
                $method->CheckMethod();
        

                $multiple = new MultipleCurrency($from,$array_converter);
                $symbols = $multiple->setCurrency();
                $amounts =$multiple->setAmount();
                return array($symbols,$amounts,true);
                  
                
            }else{
                throw new InputDataException("Too few arguments or too many arguments !!");
            } 
            
        }else{
           
            $this->args = explode(" ",$input);
            if(count($this->args) > 4 && count($this->args) < 6){
                $method = new ArgumentConvert($this->args[0],$this->args[3]);
                $method->CheckMethod();
                $symbol = new ArgumentCurrency($this->args[1],$this->args[4]);
                   
                $amount = new ArgumentAmount($this->args[2]);
                
                return array($symbol,$amount,false);
            }else{
                throw new InputDataException("Too few arguments or too many arguments !!");
            }
            
            
        }
        
        
    }
    /**
     * Clear multiple e.g ("arg1", "arg2", "arg3")
     * @param string $input
     */
    public function clearMultipleArgs($input){
        $input = str_replace($this->array[0], " ", $input);
        $input = str_replace($this->array[1], " ", $input);
        $input = str_replace("(", " ", $input);
        $input = str_replace(")", " ", $input);
        $input = preg_replace('/\s\s+/', ' ', $input);
        $this->args = explode(" ",trim($input));    
   }
}
