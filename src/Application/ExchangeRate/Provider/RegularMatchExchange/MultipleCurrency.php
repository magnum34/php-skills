<?php



namespace Application\ExchangeRate\Provider\RegularMatchExchange;
use Application\Exception\InputDataException;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentConvert;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentCurrency;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentAmount;

/**
 * Support conversion of multiple currency pairs
 */
class MultipleCurrency {
    
    /**
     * inside ("arg1","arg2","arg3" )
     * @var array 
     */
    private $from = array();
    /**
     * inside  "arg1  arg2"
     * @var array
     */
    private $fromShare = array();
    /**
     * to conversion ("arg1","arg2","arg3" ) 
     * @var array 
     */
    private $conversions = array();
    
    public function __construct($from,$conversions) {
        $this->from = $this->Share($from);
        $this->conversions = $this->Share($conversions);
        $this->Compare();
        foreach ($this->from as $value){
            $data = $this->ShareCurrencyAndSymbol($value);
            array_push($this->fromShare, $data);
        }
        

    }
    /**
     * Check the quantity of arguments and compare them
     * @throws InputDataException
     */
    public  function Compare(){
        if(count($this->from) != count($this->conversions)){
            throw new InputDataException("Wrong command Multiple currency");
        }
    }
    /**
     * Share arguments in brackets
     * @param type $string
     * @return string
     */
    public function Share($string){
        preg_match_all('/"([^"]+)"/', $string, $data);
        return $data[1];
    }
    /**
     * Share on Symbol and Currency
     * @param type $string
     * @return array(symbol,amount)
     * @throws InputDataException
     */
    public function ShareCurrencyAndSymbol($string){
        $data = explode(" ",$string);
        if(count($data) != 2 ){
           throw new InputDataException("Wrong command Multiple currency"); 
        }
        return $data;
    }
    /**
     * set currency array
     * @return array
     */
    public function setCurrency(){
        $array_currency = array();
        for($i = 0; $i < count($this->conversions); $i++){
            $obj = new ArgumentCurrency($this->fromShare[$i][0],$this->conversions[$i]);
            array_push($array_currency, $obj);
        }
        return $array_currency;
    }
    /**
     * set currency array
     * @return array
     */
    public function setAmount(){
        $array_amount = array();
        for($i = 0; $i < count($this->conversions); $i++){
            $obj = new ArgumentAmount($this->fromShare[$i][1]);
            array_push($array_amount, $obj);
        }
        return $array_amount;
    }
}
