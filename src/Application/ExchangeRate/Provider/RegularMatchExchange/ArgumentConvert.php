<?php

namespace Application\ExchangeRate\Provider\RegularMatchExchange;
use Application\ExchangeRate\Provider\Settings\Exec;
use Application\Exception\InputDataException;

/**
 * 
 * Unique arguments 
 * Unique argument or command: convert, przelicz, to, na 
 */
class ArgumentConvert {
    
    private $language = 'EN';
    private $arg1 = '';
    private $arg2 = '';
    private $isCovert = false;
    private $isBetween = false;
    
    public function __construct($arg1,$arg2) {
        $this->arg1 = $arg1;
        $this->arg2 = $arg2;
    }
    /**
     * Check that everything is correct
     * Exec::METHOD: convert, przelicz
     * Exec::BETWEEN: to, na
     * @throws InputDataException
     */
    public function CheckMethod(){
        foreach (Exec::METHOD  as $key => $item){
            if(strtoupper($item) == $this->arg1){
                $this->language = $key;
                $this->isCovert = true;
            }
        }
        foreach (Exec::BETWEEN as $key => $item){
            if(strtoupper($item) == $this->arg2 && $this->language == $key){
                $this->isBetween = true;
            }
        }
        if(!$this->isBetween && !$this->isCovert){
            throw new InputDataException("Wrong command !!");
        }
        
    }
    /**
     * get language e.g PL, EN
     * @return string
     */
    public function getLanguage(){
        return $this->language;
    }
}
