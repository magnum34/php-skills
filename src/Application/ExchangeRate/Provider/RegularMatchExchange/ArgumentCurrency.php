<?php


namespace Application\ExchangeRate\Provider\RegularMatchExchange;
use Application\ExchangeRate\Provider\Settings\Currency;
use Application\Exception\InputDataException;
/**
 *Argument Currency Symbol 
 */
class ArgumentCurrency {
    /**
     * Base Currency symbol
     * @var string 
     */
    private $currencyIn = '';
    /**
     * Quoted currency symbol
     * @var string
     */
    private $currencyOut = '';
    /**
     * Table Support only these currency pairs
     * @var array() 
     */
    private $currencyTable = array();
    

    
    public function __construct($currencyIn = null,$currencyOut = null){
        if($currencyIn != null && $currencyOut != null){
            $this->init($currencyIn, $currencyOut);
        }   
    }
    
    public function init($currencyIn,$currencyOut){
        $this->currencyIn = $currencyIn;
        $this->currencyOut = $currencyOut;
        $this->foundCurrency($this->currencyIn);
        $this->foundCurrency($this->currencyOut);
        $this->GenerationOnlyPairs();
        $this->foundOnlyPairs();
    }
    /**
     * Check whether there is such a currency symbol
     * @param string $currency
     * @return boolean
     * @throws InputDataException
     */
    public function foundCurrency($currency){
        foreach (Currency::SYMBOL as $key => $value){
            if($currency == $key){
                return true;
            }
        }
        throw new InputDataException("Not found currency symbol");
    }
    /**
     * Generation table support only these currency pairs
     */
    public function GenerationOnlyPairs(){
        foreach(Currency::SYMBOL as $input => $value){
            foreach(Currency::SYMBOL as $output => $value2){
                $inside = array();
                if($input != $output){
                    array_push($inside, $input,$output);
                    if($input != 'EUR' || $output != 'GBP' ){
                        array_push($this->currencyTable, $inside);
                    }
                    
                }   
            }
        }
    }
    /**
     * found symbols in table pairs
     * @return boolean
     * @throws InputDataException
     */
    public function foundOnlyPairs(){
        foreach($this->currencyTable as $item){
            if($item[0] == $this->currencyIn  && $item[1] == $this->currencyOut ){
                return true;
            }
        }
        throw new InputDataException("Should support only these currency pairs");
    }
    /**
     * Base Currency symbol
     * @return string
     */
    public function getInput(){
        return $this->currencyIn;
    }
    /**
     * Quoted currency symbol
     * @return string
     */
    public function getOutput(){
        return $this->currencyOut;
    }
}
