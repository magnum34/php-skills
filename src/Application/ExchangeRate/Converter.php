<?php


namespace Application\ExchangeRate;
use Application\ExchangeRate\Provider\Settings\Currency;
use AppBundle\Entity\Exchange;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentAmount;
use Application\ExchangeRate\Provider\RegularMatchExchange\ArgumentCurrency; 

class Converter {
    
    private $exchange;
    private $symbol;
    
    public function __construct(ArgumentCurrency $arg1,ArgumentAmount $arg2,$yahoo) {
        $symbol = Currency::SYMBOL;
        $this->exchange = new Exchange();
        $this->exchange->setBaseCurrency($symbol[$arg1->getInput()]);
        $this->exchange->setQuotedCurrency($symbol[$arg1->getOutput()]);
        $this->exchange->setIntegerBase($arg2->getInteger());
        $this->exchange->setDecimalBase($arg2->getDecimal());
        $this->exchange->setRateValue($yahoo->getRateValue());
        $value = new ArgumentAmount($yahoo->setBaseCurrency($arg2->getAll()));
        $this->exchange->setIntegerQuoted($value->getInteger());
        $this->exchange->setDecimalQuoted($value->getDecimal());
        
    }
    public function getExchange(){
        return $this->exchange;
    }
}
