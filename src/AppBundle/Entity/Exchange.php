<?php

namespace AppBundle\Entity;

class Exchange {
    private $baseCurrency;
    private $integerBase;
    private $decimalBase;
    private $rateValue;
    private $quotedCurrency;
    private $integerQuoted;
    private $decimalQuoted;
    

    public function getBaseCurrency() {
        return $this->baseCurrency;
    }

    public function getIntegerBase() {
        return $this->integerBase;
    }

    public function getDecimalBase() {
        return $this->decimalBase;
    }

    public function getRateValue() {
        return $this->rateValue;
    }

    public function getQuotedCurrency() {
        return $this->quotedCurrency;
    }

    public function getIntegerQuoted() {
        return $this->integerQuoted;
    }

    public function getDecimalQuoted() {
        return $this->decimalQuoted;
    }

    public function setBaseCurrency($baseCurrency) {
        $this->baseCurrency = $baseCurrency;
    }

    public function setIntegerBase($integerBase) {
        $this->integerBase = $integerBase;
    }

    public function setDecimalBase($decimalBase) {
        $this->decimalBase = $decimalBase;
    }

    public function setRateValue($rateValue) {
        $this->rateValue = $rateValue;
    }

    public function setQuotedCurrency($quotedCurrency) {
        $this->quotedCurrency = $quotedCurrency;
    }

    public function setIntegerQuoted($integerQuoted) {
        $this->integerQuoted = $integerQuoted;
    }

    public function setDecimalQuoted($decimalQuoted) {
        $this->decimalQuoted = $decimalQuoted;
    }


}
