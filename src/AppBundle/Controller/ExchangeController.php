<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Application\Exception\InputDataException;
use Application\ExchangeRate\Converter;

class ExchangeController extends Controller
{
    /**
     * @Route("/exchange", name="exchange")
     */
    public function indexAction(Request $request)
    {
        if($request->isMethod('POST')){
            $exchanges = array();
            $form = $request->request->get('calculationForm');
            try{
               $data = $this->get('exchange_rate.regular_match_exchange')->getDataArray($form['convert']);

               //Multiple
                if($data[2]){
                    for($i = 0; $i< count($data[0]); $i++){
                        $yahoo = $this->get('exchange_rate.yahoo_exchange')->fetch($data[0][$i]->getInput(),$data[0][$i]->getOutput());
                        $converter = new Converter($data[0][$i],$data[1][$i], $yahoo);
                        array_push($exchanges, $converter->getExchange());
                    }
                }else{
                    $yahoo = $this->get('exchange_rate.yahoo_exchange')->fetch($data[0]->getInput(),$data[0]->getOutput());
                    $converter = new Converter($data[0],$data[1], $yahoo);
                    array_push($exchanges, $converter->getExchange());
                   
                }
            }catch(InputDataException $ex){
                $ex->ErrorMessage($request, $ex->getMessage());
            }
           
        }
        return $this->render('exchange/index.html.twig',array('exchanges' => isset($exchanges) ? $exchanges : NULL));
    }
}
